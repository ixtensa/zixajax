<?php

namespace IXTENSA;

/**
 * Ajax class 'member'
 */
class AjaxCustom extends ZixAjax
{

	/**
     * Initialize the object with database ??
     */
    public function __construct()
    {
        // Import Contao classes as needed
        //$this->import('Database');
        //$this->import('FrontendUser', 'User');
        parent::__construct();
    }

    // Test function
    public function test()
    {
        return 'Test: successful';
    }
}