<?php

namespace IXTENSA;

/**
 * Frontend ajax handler
 */
class FrontendAjax extends \PageRegular
{

    /**
     * Initialize the object (do not remove)
     */
    public function __construct()
    {
        // Load the user object before calling the parent constructor
        $this->import('FrontendUser', 'User');
        parent::__construct();

        // Check whether a user is logged in
        define('BE_USER_LOGGED_IN', $this->getLoginStatus('BE_USER_AUTH'));
        define('FE_USER_LOGGED_IN', $this->getLoginStatus('FE_USER_AUTH'));
    }


    /**
     * Run the controller
     */
    public function run()
    {
      
        // Since initialize.php only checks the request token if there is any POST data,
        // it is possible to perform an POST/GET request without any POST vars and get
        // for example the TL_CONFIG. So if there is no POST/GET data we exit here.
        // Otherwise if there is POST data available, request is secured through the
        // Contao request token system.
        if (!$_POST && !$_GET)
        {
            header('HTTP/1.1 412 Precondition failed');
            die('Invalid ajax call.');
        }

        // Convert all variables to cleaned POST variables
        $this->convertToPostAndCleanUp();

        // Authenticate the user
        $this->User->authenticate();

        // Check if there is a page ID
        if (\Input::post('REQUEST_PAGE') > 0)
        {
            global $objPage;
            $intPage = \Input::post('REQUEST_PAGE');
            $objPage = \PageModel::findWithDetails($intPage);

            // Exit if no page details can be loaded
            if (null === $objPage)
            {
                header('HTTP/1.1 412 Precondition failed');
                die('No page object could be loaded.');
            }

            // Exit if page is not published
            if (!strlen($objPage->published) || !strlen($objPage->rootIsPublic))
            {
                header('HTTP/1.1 412 Precondition failed');
                die('Page not published.');
            }

            // Show page to guests only
            if (strlen($objPage->guests) && FE_USER_LOGGED_IN && !BE_USER_LOGGED_IN && !$objPage->protected)
            {
                header('HTTP/1.1 412 Precondition failed');
                die('Request page is for guests only.');
            }

            // Protected page
            if (strlen($objPage->protected) && !BE_USER_LOGGED_IN)
            {
                // Check if user is logged in
                if (false !== FE_USER_LOGGED_IN)
                {
                    header('HTTP/1.1 412 Precondition failed');
                    die('Request page is protected.');
                }

                // Check if user is within allowed groups
                $groups = deserialize($objPage->groups);

                if (empty($groups) || !is_array($groups) || count(array_intersect($groups, $this->User->groups)) < 1)
                {
                    header('HTTP/1.1 412 Precondition failed');
                    die('Request page is forbidden.');
                }
            }

            // Page is shown from-to
            if (!BE_USER_LOGGED_IN && ($objPage->start != '' && $objPage->start > time()) || ($objPage->stop != '' && $objPage->stop < time()))
            {
                header('HTTP/1.1 412 Precondition failed');
                die('Request page is forbidden.');
            }

            // Load page config
            $objPage = $this->loadPageConfig($objPage);
        }

        // Set language from _GET
        if (strlen(\Input::post('REQUEST_LANGUAGE')))
        {
            $GLOBALS['TL_LANGUAGE'] = \Input::post('REQUEST_LANGUAGE');
        }

        // Disallow special hooks
        unset($GLOBALS['TL_HOOKS']['outputFrontendTemplate']);
        unset($GLOBALS['TL_HOOKS']['parseFrontendTemplate']);

        // Load the default language file
        \System::loadLanguageFile('default');

        // Check if there is a controller
        if (\Input::post('REQUEST_CONTROLLER') == '')
        {
            header('HTTP/1.1 412 Precondition Failed');
            die('Invalid AJAX controller.');
        }

        // Load the AJAX controller
        $strController = ZixAjax::getClassForController(\Input::post('REQUEST_CONTROLLER'));
        $objController = new $strController();

        // Check if there is a method
        if (\Input::post('REQUEST_METHOD') == '')
        {
            header('HTTP/1.1 412 Precondition Failed');
            die('Invalid AJAX method.');
        }

        if (!method_exists($objController, \Input::post('REQUEST_METHOD')))
        {
            header('HTTP/1.1 412 Precondition Failed');
            die('AJAX method not implemented.');
        }

        // Execute the AJAX method
        $strMethod = \Input::post('REQUEST_METHOD');
        $varOutput = $objController->$strMethod();

        // Exit here
        $this->output($varOutput);

        header('HTTP/1.1 412 Precondition Failed');
        die('Invalid AJAX call.');
    }


    /**
     * Replace insert tags and output data either in raw format or json encoded.
     * @param   mixed   $varOutput
     * @return  mixed   $varOutput
     */
    protected function output($varValue)
    {
        // Output as raw format to use another type then JSON
        if (is_array($varValue) && $varValue['raw'])
        {
            header('Content-Type: ' . $varValue['header']);
            echo $this->replaceTags($varValue['content']);
            exit;
        }

        // Output as JSON
        $varValue = array
        (
            'token'     => REQUEST_TOKEN,
            'content'   => $this->replaceTags($varValue),
            'tstamp'    => time()
        );

        header('Content-Type: application/json');
        echo json_encode($varValue);
        exit;
    }


    /**
     * Recursivly replace insert tags
     */
    protected function replaceTags($varValue)
    {
        if (is_array($varValue))
        {
            foreach ($varValue as $k => $v)
            {
                $varValue[$k] = $this->replaceTags($v);
            }

            return $varValue;
        }
        elseif (is_object($varValue))
        {
            return $varValue;
        }

        return $this->replaceInsertTags($varValue);
    }


    /**
     * Convert all GET to POST and clean up the POST vars
     */
    protected function convertToPostAndCleanUp()
    {
        // $_POST
        if (!is_array($_GET) && is_array($_POST))
        {
            foreach ($arrPost as $k => $v)
            {
                \Input::setPost($k, \Input::post($k));
            }

            return;
        }

        // $_GET
        if (!empty($_GET))
        {
            foreach ($_GET as $k => $v)
            {
                \Input::setPost($k, \Input::get($k));
                unset($_GET[$k]);
            }
        }

        return;
    }


    /**
     * Load system configuration into page object
     * @param \Database\Result
     */
    protected function loadPageConfig($objPage)
    {
        // Use the global date format if none is set
        if ($objPage->dateFormat == '')
        {
            $objPage->dateFormat = $GLOBALS['TL_CONFIG']['dateFormat'];
        }

        if ($objPage->timeFormat == '')
        {
            $objPage->timeFormat = $GLOBALS['TL_CONFIG']['timeFormat'];
        }

        if ($objPage->datimFormat == '')
        {
            $objPage->datimFormat = $GLOBALS['TL_CONFIG']['datimFormat'];
        }

        // Set the admin e-mail address
        if ($objPage->adminEmail != '')
        {
            list($GLOBALS['TL_ADMIN_NAME'], $GLOBALS['TL_ADMIN_EMAIL']) = \System::splitFriendlyName($objPage->adminEmail);
        }
        else
        {
            list($GLOBALS['TL_ADMIN_NAME'], $GLOBALS['TL_ADMIN_EMAIL']) = \System::splitFriendlyName($GLOBALS['TL_CONFIG']['adminEmail']);
        }

        // Define the static URL constants
        define('TL_FILES_URL', ($objPage->staticFiles != '' && !$GLOBALS['TL_CONFIG']['debugMode']) ? $objPage->staticFiles . TL_PATH . '/' : '');
        define('TL_SCRIPT_URL', ($objPage->staticSystem != '' && !$GLOBALS['TL_CONFIG']['debugMode']) ? $objPage->staticSystem . TL_PATH . '/' : '');
        define('TL_PLUGINS_URL', ($objPage->staticPlugins != '' && !$GLOBALS['TL_CONFIG']['debugMode']) ? $objPage->staticPlugins . TL_PATH . '/' : '');

        $objLayout = $this->getPageLayout($objPage);

        if (null !== $objLayout)
        {
            // Get the page layout
            $objPage->template      = strlen($objLayout->template) ? $objLayout->template : 'fe_page';
            $objPage->templateGroup = $objLayout->templates;

            // Store the output format
            list($strFormat, $strVariant) = explode('_', $objLayout->doctype);
            $objPage->outputFormat  = $strFormat;
            $objPage->outputVariant = $strVariant;
        }

        $GLOBALS['TL_LANGUAGE'] = $objPage->language;
        return $objPage;
    }
}