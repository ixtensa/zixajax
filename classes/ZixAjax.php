<?php

namespace IXTENSA;

/**
 * Ajax controller
 */
class ZixAjax extends \System
{

    /**
     * List of all ajax methods
     * @var array
     */
    protected static $arrMethods = array();


    /**
     * Register a ajax method
     * @param   string
     * @param   string
     */
    public static function register($strName, $strClass)
    {
        if ($strName == '' || $strClass == '')
        {
            throw new \Exception('$strName and $strClass must be defined');
        }

        static::$arrMethods[$strName] = $strClass;
    }


    /**
     * Unregister a ajax method
     * @param   string
     * @param   string
     */
    public static function unregister($strName)
    {
        if ($strName == '')
        {
            throw new \Exception('$strName must be defined');
        }

        unset(static::$arrMethods[$strName]);
    }


    /**
     * Get list of ajax methods
     * @return  array
     */
    public static function getMethods()
    {
        return static::$arrMethods;
    }


    /**
     * Get class name for given controller
     * @param   string
     * @return  string
     */
    public static function getClassForController($strName)
    {
        if (!strlen(static::$arrMethods[$strName]))
        {
            throw new \Exception('Controller '.$strName.' cannot be found for initialization');
        }

        return static::$arrMethods[$strName];
    }
}