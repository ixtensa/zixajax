<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @link    https://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */

/**
 * Register custom Ajax classes for the FrontendAjax Ccontroller
 * you can create as many custom classes as you wont
 * register(raw classname, full classname)
 */
IXTENSA\ZixAjax::register('AjaxCustom', 'IXTENSA\AjaxCustom');

