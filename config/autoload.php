<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @link    https://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */

/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
    'IXTENSA',
)); 

/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Classes
	'IXTENSA\ZixAjax'         			=> 'system/modules/zixAjax/classes/ZixAjax.php',
	'IXTENSA\FrontendAjax'              => 'system/modules/zixAjax/classes/FrontendAjax.php',
	'IXTENSA\AjaxCustom'              	=> 'system/modules/zixAjax/classes/AjaxCustom.php',
));


/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	'j_zixAjax'  => 'system/modules/zixAjax/templates'
));
