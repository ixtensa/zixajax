<?php

// Patch system eg. for flash upload, this allows to transmit Session-ID and User-Auth Key using GET-Paramenters
@ini_set('session.use_only_cookies', '0');
if (isset($_GET['FE_USER_AUTH']))
{
    $_COOKIE['FE_USER_AUTH'] = $_GET['FE_USER_AUTH'];
}

// ajax.php is a frontend script
define('TL_MODE', 'FE');

// Start the session so we can access known request tokens
@session_start();

// Allow do bypass the token check if a known token is passed in
if (isset($_GET['bypassToken']) && ((is_array($_SESSION['REQUEST_TOKEN'][TL_MODE]) && in_array($_POST['REQUEST_TOKEN'], $_SESSION['REQUEST_TOKEN'][TL_MODE])) || $_SESSION['REQUEST_TOKEN'][TL_MODE] == $_POST['REQUEST_TOKEN']))
{
    define('BYPASS_TOKEN_CHECK', true);
}

// Close session so Contao's initalization routine can use ini_set()
session_write_close();

// Initialize the system
require('../../../initialize.php');

// Run the controller
$ajax = new IXTENSA\FrontendAjax;
$ajax->run();